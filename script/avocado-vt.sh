#!/bin/bash
# - repo
# - tbranch
# - prid
# - arch

rpms=(git \
      gcc \
      python3-pip \
      python3-devel \
      diffutils \
      tcpdump \
      iproute \
      iputils \
      nc \
      qemu \
      qemu-img)
sudo rm -f *.rpm
sudo yum downgrade -y "${rpms[@]}" --downloadonly --downloaddir=./ --allowerasing --skip-broken --nobest || true
sudo yum install -y "${rpms[@]}" --downloadonly --downloaddir=./ --allowerasing --skip-broken --nobest
sudo rpm -Uvh --force --nodeps *.rpm || true
sudo pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
sudo pip3 install inspektor==0.4.5 autopep8 isort==4.3.21
sudo rm -rf build-vt
mkdir build-vt

cp -a "${repo}" build-vt/
cd "build-vt/${repo}"
git config --add remote.origin.fetch "+refs/pull/${prid}/head:refs/pull/${prid}/head"
git config --add remqote.origin.fetch "+refs/heads/${tbranch}:refs/remotes/origin/${tbranch}"
git fetch
# files=$(git diff "origin/${tbranch}" --name-only | while read file; do find "${file}" -name "*.py" -type f -size +0c || true; done)
# [ -z "${files}" ] || inspekt checkall --disable-lint W,R,C,E1002,E1101,E1103,E1120,F0401,I0011,E1003 --no-license-check ${files}
cd ..

for vt in avocado avocado-vt tp-qemu tp-libvirt
do
    [ -d "${vt}" ] || git clone -b "${tbranch}" "https://gitee.com/openeuler/${vt}.git"
done
! find -name "*.cfg" | xargs tail -c 1 | grep "^.$" -B 1

sed -i "s/^libvirt-python/#&/" avocado/requirements-selftests.txt
sed -i "/^branch: /d" avocado-vt/test-providers.d/io-github-autotest-qemu.ini
if [ "${repo}" = "tp-qemu" ]; then
    sed -i "/^uri: /c uri: file://$(pwd)/tp-qemu" avocado-vt/test-providers.d/io-github-autotest-qemu.ini
else
    sed -i "/^uri: /c uri: $(pwd)/tp-qemu" avocado-vt/test-providers.d/io-github-autotest-qemu.ini
    sed -i "/^uri: /a branch: ${tbranch}" avocado-vt/test-providers.d/io-github-autotest-qemu.ini
fi
sed -i "/^branch: /d" avocado-vt/test-providers.d/io-github-autotest-libvirt.ini
if [ "${repo}" = "tp-libvirt" ]; then
    sed -i "/^uri: /c uri: file://$(pwd)/tp-libvirt" avocado-vt/test-providers.d/io-github-autotest-libvirt.ini
else
    sed -i "/^uri: /c uri: $(pwd)/tp-libvirt" avocado-vt/test-providers.d/io-github-autotest-libvirt.ini
    sed -i "/^uri: /a branch: ${tbranch}" avocado-vt/test-providers.d/io-github-autotest-libvirt.ini
fi
rm -f avocado-vt/test-providers.d/io-github-spiceqa-spice.ini

cd avocado
sudo pip3 install -r requirements-selftests.txt
sudo python3 setup.py install
cd ..

cd avocado-vt
sudo pip3 install -r requirements.txt
sudo python3 setup.py install
cd ..

sudo avocado vt-bootstrap --vt-type qemu --vt-skip-verify-download-assets --yes-to-all
sudo avocado vt-bootstrap --vt-type libvirt --vt-skip-verify-download-assets --yes-to-all

[ "${arch}" = "aarch64" ] && vt_machine_type=arm64-pci || vt_machine_type=q35
sudo avocado list qemu_img.create --vt-type qemu --vt-guest-os Guest.Linux.Fedora.28 --vt-machine-type "${vt_machine_type}"
sudo avocado list virsh.define --vt-type libvirt --vt-guest-os Guest.Linux.Fedora.28 --vt-machine-type "${vt_machine_type}"
sudo avocado list virsh.define --vt-type libvirt --vt-guest-os Guest.Linux.openEuler.20.03 --vt-machine-type "${vt_machine_type}"
